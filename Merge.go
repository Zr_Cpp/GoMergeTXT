package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
)

var (
	// 目标文件名字
	purposeFileName string
	// 正则表达式
	re, _ = regexp.Compile(`\d+`)
	// 并发协程
	//DealThread = sync.WaitGroup{}
	// 文件传输通道
	dataChn = make(chan FileData, 10)
	// 章节->文件map
	idxMap = make(map[int][]byte)
)

type FileData struct {
	idx  int
	data []byte
}

func init() {
	// 文件夹名作为最终文件名
	purposeFileName = filepath.Base(Directory)
	if purposeFileName != "." {
		// 目标文件名
		purposeFileName += ".txt"
		// 全路径
		purposeFileName = filepath.Join(Directory, "..", purposeFileName)
		// 删除已有文件
		os.Remove(purposeFileName)
	} else {
		fmt.Println("directory is empty")
		return
	}
}

// 合并
func Merge() {
	// 读取文件夹内的文件
	fileInfos, _ := ioutil.ReadDir(Directory)

	// 章节号slice
	idxList := make([]int, 0)
	fileCount := 0
	// 遍历并记录
	for _, item := range fileInfos {
		// 文件名
		fileName := item.Name()
		if !isTxt(fileName) { // 如果非txt则继续
			continue
		}
		// 查找文件名的章节号
		str := re.FindString(fileName)
		// 章节号str转int
		fileIdx, _ := strconv.Atoi(str)
		// 记录
		idxList = append(idxList, fileIdx)
		// 启动
		//DealThread.Add(1)
		go readFile(fileIdx, fileName)
		fileCount++
	}
	fillMap(fileCount)
	// 章节号排序
	sort.Ints(idxList)
	// 等待map填充完毕
	//DealThread.Wait()
	merge2OneFile(idxList)
}

// 将map合并到1个文件
func merge2OneFile(idxList []int) {
	// 将map中内容合并
	tempFileHandle, _ := os.OpenFile(purposeFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	defer tempFileHandle.Close()
	for _, val := range idxList {
		tempFileHandle.Write(idxMap[val])
	}
}

// 填充到map中
func fillMap(count int) {
	// 等待文件写入
	for i := 0; i != count; i++ {
		tempFileData := <-dataChn
		idxMap[tempFileData.idx] = tempFileData.data
	}
}

// 文件是否是txt后缀
func isTxt(fileDir string) bool {
	postfixName := path.Ext(fileDir)
	return postfixName == ".txt"
}

// 读取文件内容并合并
func readFile(fileIdx int, fileName string) {
	// 打开文件
	absFilePath := filepath.Join(Directory, fileName)
	file, err := ioutil.ReadFile(absFilePath)
	if err != nil {
		fmt.Println(fileName, err)
	}
	//fmt.Println(fileIdx, fileName)

	// 每个文件后加入换行符
	file = append(file, '\n')
	// 生成内容
	tempFileData := FileData{
		idx:  fileIdx,
		data: file,
	}
	// 传输
	dataChn <- tempFileData
}
